# Import libraries
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os
import time

# some global variables that need to change as we run the program
end_of_game = None  # set if the user wins or ends the game

# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]
LED_accuracy = 32
btn_submit = 16
btn_increase = 18
buzzer = 33 #buzzer pin number
eeprom = ES2EEPROMUtils.ES2EEPROM()


#Initializing global variables
val2 = 0
currentScore = 0
prevGuess = 0
increasePressed = 0
PWM_LED = None
PWM_buzzer= None 
val1 = None
pressedGuess = False


# Print the game banner
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


# Print the game menu
def menu():
    global end_of_game, state
    global val1, val2, currentScore
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    elif option == "P":
        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        val1 = generate_number()
        while not end_of_game:
            pass
        end_of_game = None
        val2 = 0
        currentScore = 0


    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")

def initializingLEDS(state):
    for led in LED_value:
        GPIO.output(led, GPIO.HIGH if state else GPIO.LOW)

#Extra Code for Ending Game
def endGame():
    global val2, end_of_game, PWM_led
    initializingLEDS(False)
    end_of_game = True
    val1 = 0
    val2 = 0
    PWM_led.stop(0)
    
    
def display_scores(count, raw_data):
    # print the scores to the screen in the expected format
    print("There are {} scores. Here are the top 3!".format(count))
    # print out the scores in the required format
    tot = count if count<=3 else 3
    for i in range(tot):
        print("%d - %s took %d guesses" % (i+1, raw_data[i][0], raw_data[i][1]))
    pass


# Setup Pins
def setup():

    global state, PWM_led, PWM_buzzer
    GPIO.setmode(GPIO.BOARD) #Setup board mode

    # Setup regular GPIO
    for btn in LED_value:
        GPIO.setup(btn, GPIO.OUT)
    

    GPIO.setup(btn_submit, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(btn_increase, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    # Setup PWM channels
    GPIO.setup(LED_accuracy, GPIO.OUT)
    PWM_led = GPIO.PWM(LED_accuracy, 1000)
    GPIO.setup(buzzer, GPIO.OUT)
    PWM_buzzer = GPIO.PWM(buzzer, 1)
    PWM_buzzer.start(0)

    # Setup debouncing and callbacks
    GPIO.add_event_detect(btn_submit, GPIO.FALLING, callback=btn_guess_pressed)
    GPIO.add_event_detect(btn_increase, GPIO.FALLING, callback=btn_increase_pressed)

    givenMode(state)

# Load high scores
def fetch_scores():
    # get however many scores there are
    score_count = None

    # Get the scores
    score_count = eeprom.read_block(0, 1)[0]
    scores = []

    for i in range(score_count):
        score = eeprom.read_block(i+1, 4)

        # convert the codes back to ascii
        name = chr(score[0]) + chr(score[1]) + chr(score[2])
        scores.append([name, score[3]])

    

    # return back the results
    return score_count, scores

# Save high scores
def save_scores():
    
    name = input("You have won! What is your name ?\n")
    
    # fetch scores
    count, scores = fetch_scores()

    # include new score
    scores.append(
            [name if len(name)<=3 else name[:3], currentScore])
            

    # sort
    scores.sort(key=lambda x: x[1])

    # update total amount of scores
    eeprom.clear(2048)
    eeprom.write_block(0, [len(scores)] )

    # write new scores
    for i, score in enumerate(scores):
            data_to_write = []
            # get the string
            for letter in score[0]:
                data_to_write.append(ord(letter))
            data_to_write.append(score[1])
            eeprom.write_block(i+1, data_to_write)
    pass


# Generate guess number
def generate_number():
    return random.randint(0, pow(2, 3)-1)


# Increase button pressed
def btn_increase_pressed(channel):
    global increasePressed
    global val2

    curr_t = int(round(time.time()*1000))

    if (curr_t - increasePressed>200):
        increasePressed = curr_t
        
        if (val2<7):
            val2+=1
        else:
            val2=0
        binary = bin(val2).replace("0b", "")
        binary = binary[::-1]
        arr = [0,0,0]
    
        #looping
        for i in range(len(binary)):
            arr[i] = int(binary[i])
    
        for ledNo in range(len(LED_value)):
            GPIO.output(LED_value[ledNo], arr[ledNo])        


# Guess button
def btn_guess_pressed(channel):
    # If they've pressed and held the button, clear up the GPIO and take them back to the menu screen
    global prevGuess, pressedGuess, currentScore


    curr_t = int(round(time.time()*1000))

    if (not pressedGuess and not GPIO.input(btn_submit)):
        if (curr_t - prevGuess > 200):
            prevGuess = curr_t
            
            pressedGuess = True
        


    elif (pressedGuess and GPIO.input(btn_submit)):
        if (curr_t - prevGuess < 1000):
            prevGuess = curr_t
            pressedGuess = False
            currentScore+=1
            
            accuracy_leds()

        else:
            prevGuess = curr_t
            pressedGuess = False
            
            endGame()
            trigger_buzzer()



# LED Brightness
def accuracy_leds():
    global val2, val1
    accuracy1 = 0

    if (val2 <= val1):
        accuracy1 = val2/val1

    else:
        accuracy1 = ((7 - val2) / (7 - val1)) * 100
   
    PMW_led.start(accuracy1)
    trigger_buzzer()

    if (accuracy1==100):
        endGame()
        save_scores()

# Sound Buzzer
def trigger_buzzer():
    global val2, val1, PWM_buzzer
    
    PWM_buzzer.stop()
    #defining the frequencies depending on how far off the user is
    if abs(val1- val2) == 1:
        PWM_buzzer.ChangeFrequency(0.25)  
        PWM_buzzer.start(50)
    elif abs(val1 - val2) == 2:
        PWM_buzzer.ChangeFrequency(0.5)
        PWM_buzzer.start(50)
    elif abs(val1 - val2) == 3:
        PWM_buzzer.ChangeFrequency(1)
        PWM_buzzer.start(50)


if __name__ == "__main__":
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
